using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trampaMovimiento : MonoBehaviour
{
    public float velocidadInicial = 2f;
    public float incrementoVelocidad = 0.5f;
    public float intervalo = 2f;
    public float altura = 5f;
    public float limiteSuperior = 10f;
    public float limiteInferior = -10f;

    private float tiempoActual;
    private Vector3 posicionInicial;
    private float velocidadActual;
    private bool moviendoseArriba;

    private void Start()
    {
        tiempoActual = intervalo;
        posicionInicial = transform.position;
        velocidadActual = velocidadInicial;
        moviendoseArriba = true;
    }

    private void Update()
    {
        tiempoActual += Time.deltaTime;

        if (tiempoActual >= intervalo)
        {
            tiempoActual = 0f;
            CambiarDireccion();
        }

        velocidadActual += incrementoVelocidad * Time.deltaTime;
        float velocidadMovimiento = velocidadActual * (moviendoseArriba ? 1f : -1f);
        float movimientoY = velocidadMovimiento * Time.deltaTime;

        Vector3 nuevaPosicion = transform.position + new Vector3(0f, movimientoY, 0f);
        nuevaPosicion.y = Mathf.Clamp(nuevaPosicion.y, limiteInferior, limiteSuperior);
        transform.position = nuevaPosicion;
    }

    private void CambiarDireccion()
    {
        moviendoseArriba = !moviendoseArriba;
    }

}
