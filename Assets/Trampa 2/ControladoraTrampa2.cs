using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladoraTrampa2 : MonoBehaviour
{
    public Transform Firepoint;
    public GameObject BalaPrefab;
    public float ratioInicial = 2f; 
    public float ratioDecrecion = 0.1f;
    public float velocidadInicial = 5f; 
    public float incrementoVelocidad = 1f; 
    private Transform player;
    private float ratioActual; 
    private float velocidadActual; 

    private void Start()
    {
        ratioActual = ratioInicial;
        velocidadActual = velocidadInicial;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Dispara());
        }
    }

    private void Update()
    {
        if (ratioActual > 0f)
        {
            ratioActual -= Time.deltaTime;
        }
    }

    private IEnumerator Dispara()
    {
        while (true)
        {
            if (ratioActual <= 0f)
            {
                GameObject bullet = Instantiate(BalaPrefab, Firepoint.position, Quaternion.identity);
                Vector2 direction = (player.position - transform.position).normalized;
                bullet.GetComponent<Rigidbody2D>().velocity = direction * velocidadInicial;
                ratioActual = ratioInicial;
                velocidadInicial += incrementoVelocidad; 
            }
            yield return null;
        }
    }
}

