using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_Player : MonoBehaviour
{
    void OnEnable()
    {
        //Funcion Destroy, para destruir objetos Destroy(objeto adjunto del script, tiempo);
        Destroy(gameObject, 3f);
    }


    //Sirve para detectar colliders 2d en objetos 2D, el objeto debe tener check en IS TRIGGER..
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Bullet")
        {
            //Destruimos este objeto si colisiono contra algo.
            Destroy(gameObject);
        }


        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
        }
    }
}
