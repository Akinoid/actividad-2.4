using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPlayer : MonoBehaviour
{
    public Rigidbody2D _rigidBody2D;

    public float velX;
    public float velY;
    public float moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        velX = Input.GetAxisRaw("Horizontal") * moveSpeed;
        velY = Input.GetAxisRaw("Vertical") * moveSpeed;

        _rigidBody2D.velocity = new Vector2(velX, velY);

    }
}

