using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoEnemigo1 : MonoBehaviour
{
    public Transform jugador;  // Referencia al objeto jugador
    public float tiempoMovimiento = 2f;  // Duraci�n del movimiento hacia el jugador (en segundos)
    public float tiempoDetenido = 3f;  // Duraci�n de la pausa despu�s de moverse (en segundos)

    private bool enMovimiento = true;
    private float tiempoInicioMovimiento;
    private float tiempoInicioPausa;

    private void Start()
    {
        tiempoInicioMovimiento = Time.time;
        tiempoInicioPausa = tiempoInicioMovimiento + tiempoMovimiento;
    }

    private void Update()
    {
        if (enMovimiento)
        {
            // Calcula la posici�n hacia la cual moverse (jugador)
            Vector3 direccion = (jugador.position - transform.position).normalized;

            // Calcula la velocidad de movimiento
            float duracionActual = Time.time - tiempoInicioMovimiento;
            float velocidad = Vector3.Distance(jugador.position, transform.position) / tiempoMovimiento * duracionActual;

            // Mueve al enemigo hacia el jugador
            transform.position += direccion * velocidad * Time.deltaTime;

            // Comprueba si ha pasado el tiempo de movimiento
            if (Time.time >= tiempoInicioMovimiento + tiempoMovimiento)
            {
                // Detiene el movimiento y comienza la pausa
                enMovimiento = false;
                tiempoInicioPausa = Time.time;
            }
        }
        else
        {
            // Comprueba si ha pasado el tiempo de pausa
            if (Time.time >= tiempoInicioPausa + tiempoDetenido)
            {
                // Comienza el movimiento nuevamente
                enMovimiento = true;
                tiempoInicioMovimiento = Time.time;
            }
        }
    }
}
