using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desaparece : MonoBehaviour
{
    public float tiempoDesaparicion = 2f;  // Tiempo despu�s del cual la bala desaparecer� (en segundos)

    private void Start()
    {
        // Inicia una corutina para desaparecer la bala despu�s del tiempo especificado
        StartCoroutine(DesaparecerDespuesDeTiempo(tiempoDesaparicion));
    }

    private IEnumerator DesaparecerDespuesDeTiempo(float tiempo)
    {
        // Espera el tiempo especificado
        yield return new WaitForSeconds(tiempo);

        // Destruye la bala
        Destroy(gameObject);
    }
}
