using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparo : MonoBehaviour
{
    public GameObject balaPrefab;  // Prefab de la bala
    public Transform puntoDisparo;  // Punto de origen de la bala
    public Transform jugador;  // Referencia al objeto del jugador
    public float tiempoMovimiento = 2f;  // Tiempo de movimiento hacia el jugador
    public float tiempoDetenido = 3f;  // Tiempo de pausa antes de disparar
    public float frecuenciaInicial = 1f;  // Frecuencia de disparo inicial (balas por segundo)
    public float incrementoFrecuencia = 0.1f;  // Incremento de frecuencia con el tiempo (balas por segundo)

    private bool enMovimiento = true;
    private float tiempoInicioMovimiento;
    private float tiempoUltimoDisparo;
    private float frecuenciaDisparo;

    private void Start()
    {
        tiempoInicioMovimiento = Time.time;
        tiempoUltimoDisparo = Time.time;
        frecuenciaDisparo = frecuenciaInicial;
    }

    private void Update()
    {
        if (enMovimiento)
        {
            if (Time.time >= tiempoInicioMovimiento + tiempoMovimiento)
            {
                enMovimiento = false;
                tiempoInicioMovimiento = Time.time;
            }
        }
        else
        {
            if (Time.time >= tiempoInicioMovimiento + tiempoDetenido)
            {
                enMovimiento = true;
                tiempoInicioMovimiento = Time.time;
                frecuenciaDisparo += incrementoFrecuencia;
            }
            else if (Time.time >= tiempoUltimoDisparo + (1f / frecuenciaDisparo))
            {
                Disparar();
                tiempoUltimoDisparo = Time.time;
            }
        }
    }

    private void Disparar()
    {
        GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, puntoDisparo.rotation);
        Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();

        Vector2 direccion = (jugador.position - puntoDisparo.position).normalized;
        balaRigidbody.velocity = direccion * 10f;
    }


}
