using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class movimientoPrue : MonoBehaviour
{
    public Rigidbody2D _rigidBody2D;

    public float velX;
    public float velY;
    public float moveSpeed;


    public TextMeshProUGUI VidaText;
    public int vida = 100;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();


        VidaText.text = "" + vida + "%";
    }

    // Update is called once per frame
    void Update()
    {
        velX = Input.GetAxisRaw("Horizontal") * moveSpeed;
        velY = Input.GetAxisRaw("Vertical") * moveSpeed;

        _rigidBody2D.velocity = new Vector2(velX, velY);





        VidaText.text = "" + vida + "%";


        if(vida <= 0)
        {
            Destroy(gameObject);
        }

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bala")
        {
            vida -= 10;
            Destroy(collision.gameObject);
        }
    }
}





