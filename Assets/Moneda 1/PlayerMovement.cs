using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;

    private void Update()
    {
        // Obtener la entrada de movimiento horizontal y vertical
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // Calcular la direcci�n del movimiento
        Vector3 moveDirection = new Vector3(moveHorizontal, moveVertical, 0f);

        // Normalizar la direcci�n para evitar movimientos diagonales m�s r�pidos
        moveDirection.Normalize();

        // Mover el jugador en funci�n de la direcci�n y la velocidad de movimiento
        transform.position += moveDirection * moveSpeed * Time.deltaTime;
    }
}
