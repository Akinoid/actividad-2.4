using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    private int score = 0;

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verificar si el objeto colisionado es un objeto de puntuaci�n
        Coin pointObject = other.GetComponent<Coin>();
        if (pointObject != null)
        {
            // Obtener la puntuaci�n del objeto y a�adirla al puntaje del jugador
            int points = pointObject.GetPoints();
            score += points;

            // Destruir el objeto de puntuaci�n
            Destroy(other.gameObject);

            // Imprimir el puntaje actual en la consola
            Debug.Log("Puntaje actual: " + score);
        }
    }
}
