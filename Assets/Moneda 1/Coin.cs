using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int initialPoints = 10;
    public float pointDecayTime = 5f;

    private int currentPoints;
    private bool isDecaying = false;

    private void Start()
    {
        currentPoints = initialPoints;
        Invoke("DecayPoints", pointDecayTime);
    }

    private void DecayPoints()
    {
        currentPoints--;
        if (currentPoints > 0)
        {
            Invoke("DecayPoints", pointDecayTime);
        }
        else
        {
            isDecaying = false;
        }
    }

    public int GetPoints()
    {
        return currentPoints;
    }

    public void StartDecay()
    {
        if (!isDecaying)
        {
            isDecaying = true;
            Invoke("DecayPoints", pointDecayTime);
        }
    }
}
