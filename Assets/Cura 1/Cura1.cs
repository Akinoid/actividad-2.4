using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cura1 : MonoBehaviour
{
    public int healAmount = 10;
    public float decayTime = 5f;

    private bool isDecaying = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Llama a la función de curación del jugador
            collision.GetComponent<PlayerHealth>().Heal(healAmount);

            // Inicia la degradación del objeto de curación
            StartDecay();
        }
    }

    private void StartDecay()
    {
        if (!isDecaying)
        {
            isDecaying = true;
            Invoke("DecayObject", decayTime);
        }
    }

    private void DecayObject()
    {
        // Realiza acciones adicionales antes de que el objeto se degrade completamente, si es necesario

        // Destruye el objeto de curación
        Destroy(gameObject);
    }
}
