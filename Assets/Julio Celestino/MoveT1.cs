using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveT1 : MonoBehaviour
{
    public float speed;
    public bool up;
    public float contT;
    public float TimeO;
    public float MaxTurbo;
    public float TimeTurbo;

    // Start is called before the first frame update
    void Start()
    {
        contT = TimeO;
        MaxTurbo = TimeTurbo;
    }

    // Update is called once per frame
    void Update()
    {
        if (up == true)
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (up == false)
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        contT -= Time.deltaTime;
        if (contT <= 0)
        {
            contT = TimeO;
            
            up = !up;
        }
        MaxTurbo -= Time.deltaTime;
        if (MaxTurbo <= 0)
        {
            MaxTurbo = TimeTurbo;
            speed += 1;
        }
    }
}
